# Ruby Tutorial
Ruby Tutorial focusing on Data/Text Processing.

Based on **Ruby Data Processing** Using Map, Reduce and Select *By Jay Godse* https://amzn.to/2NSxIXW
 

## to run or modify
Done using Junyper Notebooks. See https://www.anaconda.com/ and https://github.com/SciRuby/iruby to install.

Use `iruby notebook` or `jupyter notebook` to start.

## Basics

### Strings
DataProcessingBasics_Strings.ipynb

### Arrays
DataProcessingBasics_Arrays.ipynb

### Hashes
DataProcessingBasics_Hash.ipynb

### Block Syntax
DataProcessingBasics_BlockSyntax.ipynb

### Reading from Files
DataProcessingBasics_FileReading.ipynb

## Overview

### Map

### Reduce

### Select

## Solutions

### Debugging Blocks

### FizBuzz

### Sum of odd Cubes

### Sort a list of names to csv
